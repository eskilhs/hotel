public class Room {

  private static int room_id_generator = 0;

  private int room_id;
  private int capasity;
  private int price;
  private String type;

  public Room(int capasity, int price, String type){
	room_id_generator ++;
	room_id = room_id_generator;
    this.capasity = capasity;
    this.price = price;
    this.type = type;
  }

  public int getCapasity() {
    return capasity;
  }

  public int getPrice() {
    return price;
  }

  public String getType() {
    return type;
  }

  public int getRoom_id() {
	  return room_id;
  }

}