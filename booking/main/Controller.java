import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Controller {

    String[] roomTypeList = {"Bryllupssuite", "Businessuite", "Kvalitetsrom", "Lavprisrom"};
    private List<Room> registreredRoomList = new ArrayList<>();
    private List<Booking> bookedRoomList = new ArrayList<>();
    
    public boolean registerRoom(int capasity, int price, String type) {
        for (int i = 0; i < roomTypeList.length; i++) {
            if (type.equalsIgnoreCase(roomTypeList[i])) {
                Room room = new Room(capasity, price, type);
                return registreredRoomList.add(room); 
            }
        }
        System.out.println("Type rom finnes ikke");
        return false;
    }

    public boolean bookRoom(int room_id, int numberOfPersons, int bookingDate ) {
        if (room_id <= registreredRoomList.size()) {
            if (numberOfPersons <= registreredRoomList.get(room_id-1).getCapasity()) {
                for (Booking booking : bookedRoomList) {
                    if (booking.getRoom_id() == room_id && booking.getBookingDate() == bookingDate ) {
                        System.out.println("Rommet er allerde reservert paa denne datoen");
                        return false;    
                    }
                }
                Booking booking = new Booking(room_id, numberOfPersons, bookingDate);
                bookedRoomList.add(booking);
                return true;
            }
            else{
                System.out.println("Ikke nok plasser paa dette rommet");
                return false;
            }
        }
        else{
            System.out.println("Rom finnes ikke");
            return false;
        }
        
    }

    public int fetchTotalRoomIncome(int room_id) {
        int totalBookingsByRoom = 0;
        int roomPrice = registreredRoomList.get(room_id-1).getPrice();

        for (Booking booking : bookedRoomList) {
            if (booking.getRoom_id() == room_id) {
                totalBookingsByRoom ++; 
            }
        }
        int totalIncome = totalBookingsByRoom * roomPrice;
        return totalIncome;
    }

    public List<Room> allRoomsByIncome() {
        List<Room> roomsSortedByIncome = new ArrayList<>();
        Collections.copy(roomsSortedByIncome, registreredRoomList);
        roomsSortedByIncome.sort(new IncomeSorter().reversed());
        return roomsSortedByIncome;
    }

    public class IncomeSorter implements Comparator<Room> {

        @Override
        public int compare(Room o1, Room o2) {
            Integer a = Integer.valueOf(fetchTotalRoomIncome(o2.getRoom_id()));
            Integer b = Integer.valueOf(fetchTotalRoomIncome(o1.getRoom_id()));
            return a.compareTo(b);
        }
    }

    public int nrOfDaysBookedStraight(int room_id) {
        int nrOfDays = 0;
        List<Integer> bookedRoomListById = new ArrayList<>();

        for (Booking booking : bookedRoomList) {
            if (booking.getRoom_id() == room_id) {
                bookedRoomListById.add(booking.getBookingDate());
            }
        }
        Collections.sort(bookedRoomListById);
        int temp1 = 0;
        for (Integer integer : bookedRoomListById) {
            if (integer == temp1+1) {
                nrOfDays++;
            }
            temp1 = integer;
        }
        return nrOfDays;   
    }
}
