

public class ControllerTest {
    static Controller controller = new Controller();

    public static void main(String[] args) {

        boolean registerdRoom1 = controller.registerRoom(3, 1000, "Kvalitetsrom");
        boolean registerdRoom2 = controller.registerRoom(2, 2000, "Bryllupssuite");
        boolean registerdRoom3 = controller.registerRoom(2, 3000, "Businessuite");
        boolean registerdRoom4 = controller.registerRoom(4, 500, "Lavprisrom");
        boolean registerdRoom5 = controller.registerRoom(3, 1000, "Kvalitetsrom");
        boolean registerdRoom6 = controller.registerRoom(3, 5000, "Businessuite");
        boolean registerdRoom7 = controller.registerRoom(4, 2000, "Kvalitetsrom");
        boolean registerdRoom8 = controller.registerRoom(2, 10000, "Bryllupssuite");
        boolean registerdRoom9 = controller.registerRoom(2, 2000, "Kvalitetsrom");
        boolean registerdRoom10 = controller.registerRoom(4, 5000, "Businessuite");
        boolean registerdRoom11 = controller.registerRoom(3, 500, "Lavprisrom");

        System.out.println(controller.bookRoom(3, 1, 10497)); 
        System.out.println(controller.fetchTotalRoomIncome(3));
        
    }

}
