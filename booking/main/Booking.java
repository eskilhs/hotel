public class Booking {
    private int numberOfPersons;
    private int bookingDate;
    private int room_id;
    
    public Booking(int room_id, int numberOfPersons, int bookingDate){
        this.room_id = room_id;
        this.numberOfPersons = numberOfPersons;
        this.bookingDate = bookingDate;
    }

    public int getRoom_id() {
        return room_id;
    }

    public int getBookingDate() {
        return bookingDate;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

}
